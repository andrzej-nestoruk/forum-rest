# Forum REST API

## Starting

Running application:

```
sbt run
```

To run application AND generate random database data execute:

```
sbt run init
```

To run test execute:

```
sbt test
```

## Configuration

`src/main/resources/application.conf` file contains configuration settings for:

- HTTP server:
    - `interface`, 
    - `port`,
    - etc
- Databases connections:
    - `serverName`, 
    - `portNumber`, 
    - `user`,
    - etc
- Other settings:
    - `maxLimit` - max number of items that can be retrieved in single API request,
    - `postsToGenerate` - number of posts that will be generated using `sbt run init`,
    - `answersToGenerate`

-----------------------------




## API endpoints:

1. [Posts](#posts)
    - [GET /posts](#get-posts)
    - [GET /posts/:id](#get-single-post)
    - [GET /posts/:id/answers](#get-posts-answers)
    - [GET /posts/:id/answers/:id](#get-posts-single-answer)
    - [POST /posts](#create-post)
    - [POST /posts/:id/answers](#create-posts-answer)
    - [PUT /posts/:id](#update-post)
    - [PUT /posts/:id/answers/:id](#update-posts-answer)
    - [DELETE /posts/:id](#delete-post)
    - [DELETE /posts/:id/answers/:id](#delete-posts-answer)
2. [Answers](#answers)
    - [GET /answers](#get-answers)
    - [GET /answers/:id](#get-single-answer)
    - [POST /answers](#create-answer)
    - [PUT /answers/:id](#update-answer)
    - [DELETE /answers/:id](#delete-answer)



# Posts

## Get Posts

### Method and URL
**GET /posts**

### Query string params

##### Optional:

`offset: [ Integer ]`

`limit: [ Integer ]`

### Example request

GET http://localhost:8080/v1/posts

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/posts",
        "next": "http://localhost:8080/v1/posts?offset=20&limit=20"
    },
    "data": [
        {
            "attributes": {
                "id": 196,
                "subject": "Kolejny temat",
                "content": "Tekst.",
                "name": "Edward",
                "email": "edward@testowy.pl",
                "createdAt": {
                    "value": "2017-01-28 12:00:00.0"
                }
            },
            "links": {
                "self": "http://localhost:8080/v1/posts/196"
            }
        },

        (...)

    ]
}
```

-----------------------------




## Get single Post

### Method and URL
**GET /posts/:id**

### Example request

GET http://localhost:8080/v1/posts/1

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/posts/1",
        "answers": "http://localhost:8080/v1/posts/1/answers"
    },
    "data": {
        "attributes": {
            "id": 1,
            "subject": "To jest temat...",
            "content": "Tekst.",
            "name": "Sebastian",
            "email": "sebastian@testowy.pl",
            "createdAt": {
                "value": "2017-01-01 12:00:00.0"
            }
        },
        "links": {
            "self": "http://localhost:8080/v1/posts/1",
            "answers": "http://localhost:8080/v1/posts/1/answers"
        }
    }
}
```

-----------------------------




## Get Post's Answers

### Method and URL
**GET /posts/:id/answers**

### Query string params

##### Optional:

`offset: [ Integer ]`

`limit: [ Integer ]`

##### OR (all these 3 parameters have to be set to trigger "Before After query"):

`id: [ Integer ]`

`before: [ Integer ]`

`after: [ Integer ]`

### Example request

GET http://localhost:8080/v1/posts/3/answers?id=463&before=2&after=1

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/posts/3/answers"
    },
    "data": [
        {
            "attributes": {
                "id": 67,
                "postId": 3,
                "content": "Tekst.",
                "name": "Ewelina",
                "email": "ewelina@testowy.pl",
                "createdAt": {
                    "value": "2017-01-11 12:00:00.0"
                }
            },
            "links": {
                "self": "http://localhost:8080/v1/posts/3/answers/67"
            }
        },
        {
            "attributes": {
                "id": 377,
                "postId": 3,
                "content": "Tekst.",
                "name": "Dariusz",
                "email": "dariusz@testowy.pl",
                "createdAt": {
                    "value": "2017-01-13 12:00:00.0"
                }
            },
            "links": {
                "self": "http://localhost:8080/v1/posts/3/answers/377"
            }
        },
        {
            "attributes": {
                "id": 463,
                "postId": 3,
                "content": "Tekst.",
                "name": "Waldemar",
                "email": "waldemar@testowy.pl",
                "createdAt": {
                    "value": "2017-01-15 12:00:00.0"
                }
            },
            "links": {
                "self": "http://localhost:8080/v1/posts/3/answers/463"
            }
        },
        {
            "attributes": {
                "id": 243,
                "postId": 3,
                "content": "Tekst.",
                "name": "Anna",
                "email": "anna@testowy.pl",
                "createdAt": {
                    "value": "2017-01-19 12:00:00.0"
                }
            },
            "links": {
                "self": "http://localhost:8080/v1/posts/3/answers/243"
            }
        }
    ]
}
```

-----------------------------




## Get Post's single Answer

### Method and URL
**GET /posts/:id/answers/:id**

### Example request

GET http://localhost:8080/v1/posts/1/answers/1

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/posts/1/answers/1"
    },
    "data": {
        "attributes": {
            "id": 1,
            "postId": 1,
            "content": "Komentarz.",
            "name": "Izabela",
            "email": "izabela@testowy.pl",
            "createdAt": {
                "value": "2017-01-01 12:00:00.0"
            }
        },
        "links": {
            "self": "http://localhost:8080/v1/posts/1/answers/1"
        }
    }
}
```

-----------------------------




## Create Post

### Method and URL
**POST /posts**

### POST data params

##### Required:

`subject: [ String ]`
`content: [ String ]`
`name: [ String ]`
`email: [ String ]`

### Example request

POST http://localhost:8080/v1/posts
```
Content-Type: application/json

{
    "subject": "Mój temat",
    "content": "Moja treść",
    "name": "Andrzej",
    "email": "andrzej@testowy.pl"
}
```

### Example response

```
201 Created

{
    "status": "ok",
    "code": 201,
    "links": {
        "self": "http://localhost:8080/v1/posts"
    },
    "data": {
        "attributes": {
            "id": 201,
            "subject": "Mój temat",
            "content": "Moja treść",
            "name": "Andrzej",
            "email": "andrzej@testowy.pl",
            "createdAt": {
                "value": "2017-03-19 23:29:04.057"
            },
            "secret": "m8IkWhPDW7hAIcwKL1BJt2QhNAvJuxdv"
        },
        "links": {
            "self": "http://localhost:8080/v1/posts"
        }
    }
}
```

-----------------------------




## Create Post's Answer

### Method and URL
**POST /posts/:id/answers**

### POST data params

##### Required:

`content: [ String ]`
`name: [ String ]`
`email: [ String ]`

### Example request

POST http://localhost:8080/v1/posts/1/answers

```
Content-Type: application/json

{
    "content": "Moja odpowiedź",
    "name": "Andrzej",
    "email": "andrzej@testowy.pl"
}
```

### Example response

```
201 Created

{
    "status": "ok",
    "code": 201,
    "links": {
        "self": "http://localhost:8080/v1/posts/201/answers"
    },
    "data": {
        "attributes": {
            "id": 501,
            "postId": 201,
            "content": "Moja odpowiedź",
            "name": "Andrzej",
            "email": "andrzej@testowy.pl",
            "createdAt": {
                "value": "2017-03-19 23:38:32.339"
            },
            "secret": "ZdG5dIQqXCME4ZzUacwJ6SUPRF6n69f0"
        },
        "links": {
            "self": "http://localhost:8080/v1/posts/201/answers"
        }
    }
}
```

-----------------------------




## Update Post

### Method and URL
**PUT /posts/:id**

### Query string params

##### Required:

`secret: [ String ]`

### POST data params

##### Required:

`subject: [ String ]`
`content: [ String ]`
`name: [ String ]`
`email: [ String ]`

### Example request

PUT http://localhost:8080/v1/posts/201?secret=m8IkWhPDW7hAIcwKL1BJt2QhNAvJuxdv

```
Content-Type: application/json

{
    "subject": "Mój temat",
    "content": "Moja *poprawiona* treść",
    "name": "Andrzej",
    "email": "andrzej@testowy.pl"
}
```

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/posts/201"
    },
    "data": {
        "attributes": {
            "id": 201,
            "subject": "Mój temat",
            "content": "Moja *poprawiona* treść",
            "name": "Andrzej",
            "email": "andrzej@testowy.pl",
            "createdAt": {
                "value": "2017-03-19 23:31:14.687"
            },
            "secret": "m8IkWhPDW7hAIcwKL1BJt2QhNAvJuxdv"
        },
        "links": {
            "self": "http://localhost:8080/v1/posts/201"
        }
    }
}
```

-----------------------------




## Update Post's Answer

### Method and URL
**PUT /posts/:id/answers/:id**

### Query string params

##### Required:

`secret: [ String ]`

### POST data params

##### Required:

`content: [ String ]`
`name: [ String ]`
`email: [ String ]`

### Example request

PUT http://localhost:8080/v1/posts/201/answers/501?secret=ZdG5dIQqXCME4ZzUacwJ6SUPRF6n69f0

```
Content-Type: application/json

{
    "content": "Moja *poprawiona* odpowiedź",
    "name": "Andrzej",
    "email": "andrzej@testowy.pl"
}
```

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/posts/201/answers/501"
    },
    "data": {
        "attributes": {
            "id": 501,
            "postId": 201,
            "content": "Moja *poprawiona* odpowiedź",
            "name": "Andrzej",
            "email": "andrzej@testowy.pl",
            "createdAt": {
                "value": "2017-03-19 23:40:30.694"
            },
            "secret": "ZdG5dIQqXCME4ZzUacwJ6SUPRF6n69f0"
        },
        "links": {
            "self": "http://localhost:8080/v1/posts/201/answers/501"
        }
    }
}
```

-----------------------------




## Delete Post

### Method and URL:
**DELETE /posts/:id**

### Query string params

##### Required:

`secret: [ String ]`


### Example request

DELETE http://localhost:8080/v1/posts/201?secret=m8IkWhPDW7hAIcwKL1BJt2QhNAvJuxdv

### Example response

```
204 No Content
```

-----------------------------




## Delete Post's Answer

### Method and URL:
**DELETE /posts/:id/asnwers/:id**

### Query string params

##### Required:

`secret: [ String ]`


### Example request

DELETE http://localhost:8080/v1/posts/201/answers/501?secret=ZdG5dIQqXCME4ZzUacwJ6SUPRF6n69f0

### Example response

```
204 No Content
```

-----------------------------




# Answers

## Get Answers

### Method and URL
**GET /answers**

### Query string params

##### Optional:

`offset: [ Integer ]`

`limit: [ Integer ]`

### Example request

GET http://localhost:8080/v1/answers

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/answers",
        "next": "http://localhost:8080/v1/answers?offset=20&limit=20"
    },
    "data": [
        {
            "attributes": {
                "id": 500,
                "postId": 30,
                "content": "Tekst.",
                "name": "Maria",
                "email": "maria@testowy.pl",
                "createdAt": {
                    "value": "2017-01-24 12:00:00.0"
                }
            },
            "links": {
                "self": "http://localhost:8080/v1/answers/500"
            }
        },

        (...)

    ]
}
```

-----------------------------




## Get single Answer

### Method and URL
**GET /answers/:id**

### Example request

GET http://localhost:8080/v1/answers/500

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/answers/500"
    },
    "data": {
        "attributes": {
            "id": 500,
            "postId": 30,
            "content": "Tekst.",
            "name": "Maria",
            "email": "maria@testowy.pl",
            "createdAt": {
                "value": "2017-01-24 12:00:00.0"
            }
        },
        "links": {
            "self": "http://localhost:8080/v1/answers/500"
        }
    }
}
```

-----------------------------




## Create Answer

### Method and URL
**POST /answer**

### POST data params

##### Required:

`postId: [ Integer ]`
`content: [ String ]`
`name: [ String ]`
`email: [ String ]`

### Example request

POST http://localhost:8080/v1/posts

```
Content-Type: application/json

{
    "postId": 1,
    "content": "Moja odpowiedź",
    "name": "Andrzej",
    "email": "andrzej@testowy.pl"
}
```

### Example response

```
201 Created

{
    "status": "ok",
    "code": 201,
    "links": {
        "self": "http://localhost:8080/v1/answers"
    },
    "data": {
        "attributes": {
            "id": 503,
            "postId": 1,
            "content": "Moja odpowiedź",
            "name": "Andrzej",
            "email": "andrzej@testowy.pl",
            "createdAt": {
                "value": "2017-03-19 23:52:46.293"
            },
            "secret": "gNX5b4WTuSzVbxUKallB172mwHvb5M3C"
        },
        "links": {
            "self": "http://localhost:8080/v1/answers"
        }
    }
}
```

-----------------------------




## Update Answer

### Method and URL
**PUT /answers/:id**

### Query string params

##### Required:

`secret: [ String ]`

### POST data params

##### Required:

`postId: [ Integer ]`
`content: [ String ]`
`name: [ String ]`
`email: [ String ]`

### Example request

PUT http://localhost:8080/v1/answers/503?secret=gNX5b4WTuSzVbxUKallB172mwHvb5M3C

```
Content-Type: application/json

{
    "postId": 1,
    "content": "Moja *poprawiona* odpowiedź",
    "name": "Andrzej",
    "email": "andrzej@testowy.pl"
}
```

### Example response

```
200 OK

{
    "status": "ok",
    "code": 200,
    "links": {
        "self": "http://localhost:8080/v1/answers/503"
    },
    "data": {
        "attributes": {
            "id": 503,
            "postId": 1,
            "content": "Moja *poprawiona* odpowiedź",
            "name": "Andrzej",
            "email": "andrzej@testowy.pl",
            "createdAt": {
                "value": "2017-03-19 23:57:37.952"
            },
            "secret": "gNX5b4WTuSzVbxUKallB172mwHvb5M3C"
        },
        "links": {
            "self": "http://localhost:8080/v1/answers/503"
        }
    }
}
```

-----------------------------




## Delete Answer

### Method and URL:
**DELETE /answers/:id**

### Query string params

##### Required:

`secret: [ String ]`


### Example request

DELETE http://localhost:8080/v1/answers/503?secret=gNX5b4WTuSzVbxUKallB172mwHvb5M3C

### Example response

```
204 No Content
```