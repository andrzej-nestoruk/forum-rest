import Dependencies._

lazy val root = (project in file(".")).
  settings(
  
    inThisBuild(List(
      organization := "pl.anestoruk",
      scalaVersion := "2.11.8",
      version := "0.1.0-SNAPSHOT"
    )),
    
    name := "Forum-REST",
    
    libraryDependencies ++= Seq(
      scalaTest % Test,
      "org.specs2" %% "specs2-core" % "2.3.11" % "test",
      "org.specs2" %% "specs2-mock" % "2.3.11",
      "com.typesafe.akka" %% "akka-http" % "10.0.4",
      "com.typesafe.akka" %% "akka-http-testkit" % "10.0.4",
      "com.typesafe.play" %% "play-json" % "2.6.0-M1",
      "com.typesafe.slick" %% "slick" % "3.2.0",
      "org.slf4j" % "slf4j-nop" % "1.6.4",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.2.0",
      "com.h2database" % "h2" % "1.4.193",
      "org.postgresql" % "postgresql" % "9.4.1212"
    )
  )

parallelExecution in Test := false