package pl.anestoruk.routes

import scala.concurrent.Future

import org.scalatest.{ Matchers, WordSpec }

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import pl.anestoruk._
import pl.anestoruk.models._
import pl.anestoruk.models.PlayJsonFormats._
import pl.anestoruk.utils.PlayJsonMarshallers._
import play.api.libs.json.JsValue
import akka.http.scaladsl.server.Route
import org.scalatest.Ignore

//@Ignore
class PostRoutesSpec extends WordSpec with Matchers with ScalatestRouteTest with RoutesContext {
  
  val baseUri = uriPrefix + "/posts"
  
  "Posts routes" should {
    
    "return Posts" in new DataContext {
      val sResult = postsList.take(20)
      val expected = sResult.map { _.toReadable }
      postServiceMock.getAllByActivity(0, 20) returns Future { sResult }
      
      Get(baseUri) ~> routes ~> check {
        val result = getAsList[PostReadable](responseAs[JsValue])
        result shouldBe expected
        status shouldBe StatusCodes.OK
      }
    }
    
    "return correct Posts with 'offset' and 'limit' parameters set" in new DataContext {
      val sResult = postsList.drop(10).take(15)
      val expected = sResult.map { _.toReadable }
      postServiceMock.getAllByActivity(10, 15) returns Future { sResult }
      
      Get(baseUri + "?offset=10&limit=15") ~> routes ~> check {
        val result = getAsList[PostReadable](responseAs[JsValue])
        result shouldBe expected
        status shouldBe StatusCodes.OK
      }
    }
    
    "return 'NotFound' for non existing Post" in new DataContext {
      val sResult = None
      postServiceMock.getOne(666) returns Future { sResult }
      
      Get(baseUri + "/666") ~> routes ~> check {
        val result = getAsError(responseAs[JsValue])
        result shouldBe notFoundError
        status shouldBe StatusCodes.NotFound
      }
    }
    
    "return existing Post" in new DataContext {
      val sResult = postsList(0)
      val expected = sResult.toReadable
      postServiceMock.getOne(postsList(0).id) returns Future { Some(sResult) }
      
      Get(baseUri + "/" + postsList(0).id) ~> routes ~> check {
        val result = getAsEntity[PostReadable](responseAs[JsValue])
        result shouldBe expected
        status shouldBe StatusCodes.OK
      }
    }
    
    "create new Post with JSON send via POST" in new DataContext {
      val sResult = newPost.toEntity(1)
      val expected = sResult.toReadable
      postServiceMock.create(newPost) returns Future { Some(sResult) }
      
      Post(baseUri, newPost) ~> routes ~> check {
        val result = getAsEntity[Post](responseAs[JsValue])
        result shouldBe sResult
        status shouldBe StatusCodes.Created
      }
    }
    
    "return 'BadRequest' for an attempt to create new Post with invalid JSON" in new DataContext {
      Post(baseUri, """{"content":"test"}""") ~> Route.seal(routes) ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }
    
    "update existing Post with JSON send via PUT" in new DataContext {
      val sResult = postsList(0).merge(newPost)
      val expected = sResult.toReadable
      postServiceMock.update(postsList(0).id, postsList(0).secret, newPost) returns Future { Some(sResult) }
      
      Put(baseUri + "/" + postsList(0).id + "?secret=" + postsList(0).secret, newPost) ~> routes ~> check {
        val result = getAsEntity[Post](responseAs[JsValue])
        result shouldBe sResult
        status shouldBe StatusCodes.OK
      }
    }
    
    "return 'NotFound' for an attempt to update Post with invalid secret" in new DataContext {
      val sResult = None
      postServiceMock.update(postsList(0).id, "INVALID_SECRET", newPost) returns Future { sResult }
      
      Put(baseUri + "/" + postsList(0).id + "?secret=" + "INVALID_SECRET", newPost) ~> Route.seal(routes) ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }
    
    "delete existing Post" in new DataContext {
      val sResult = true
      postServiceMock.delete(postsList(0).id, postsList(0).secret) returns Future { sResult }
      
      Delete(baseUri + "/" + postsList(0).id + "?secret=" + postsList(0).secret) ~> routes ~> check {
        status shouldBe StatusCodes.NoContent
      }
    }
    
    "return 'NotFound' for an attempt to delete Post with invalid secret" in new DataContext {
      val sResult = false
      postServiceMock.delete(postsList(0).id, "INVALID_SECRET") returns Future { sResult }
      
      Delete(baseUri + "/" + postsList(0).id + "?secret=" + "INVALID_SECRET") ~> Route.seal(routes) ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }
    
  }
  
}