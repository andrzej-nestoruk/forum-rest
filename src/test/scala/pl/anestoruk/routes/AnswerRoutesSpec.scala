package pl.anestoruk.routes

import scala.concurrent.Future
import org.scalatest.{ Matchers, WordSpec }
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import pl.anestoruk._
import pl.anestoruk.models._
import pl.anestoruk.models.PlayJsonFormats._
import pl.anestoruk.utils.PlayJsonMarshallers._
import play.api.libs.json.JsValue
import akka.http.scaladsl.server.Route
import org.scalatest.Ignore

//@Ignore
class AnswerRoutesSpec extends WordSpec with Matchers with ScalatestRouteTest with RoutesContext {
  
  val baseUri = uriPrefix + "/answers"
  
  "Answer routes" should {
    
    "return Answers" in new DataContext {
      val sResult = answersList.take(20)
      val expected = sResult.map { _.toReadable }
      answerServiceMock.getAll(0, 20) returns Future { sResult }
      
      Get(baseUri) ~> routes ~> check {
        val result = getAsList[AnswerReadable](responseAs[JsValue])
        result shouldBe expected
        status shouldBe StatusCodes.OK
      }
    }
    
    "return correct Answers with 'offset' and 'limit' parameters set" in new DataContext {
      val sResult = answersList.drop(10).take(15)
      val expected = sResult.map { _.toReadable }
      answerServiceMock.getAll(10, 15) returns Future { sResult }
      
      Get(baseUri + "?offset=10&limit=15") ~> routes ~> check {
        val result = getAsList[AnswerReadable](responseAs[JsValue])
        result shouldBe expected
        status shouldBe StatusCodes.OK
      }
    }
    
    "return 'NotFound' for non existing Answer" in new DataContext {
      val sResult = None
      answerServiceMock.getOne(666) returns Future { sResult }
      
      Get(baseUri + "/666") ~> routes ~> check {
        val result = getAsError(responseAs[JsValue])
        result shouldBe notFoundError
        status shouldBe StatusCodes.NotFound
      }
    }
    
    "return existing Answer" in new DataContext {
      val sResult = answersList(0)
      val expected = sResult.toReadable
      answerServiceMock.getOne(answersList(0).id) returns Future { Some(sResult) }
      
      Get(baseUri + "/" + answersList(0).id) ~> routes ~> check {
        val result = getAsEntity[AnswerReadable](responseAs[JsValue])
        result shouldBe expected
        status shouldBe StatusCodes.OK
      }
    }
    
    "create new Answer with JSON send via POST" in new DataContext {
      val sResult = newAnswer.toEntity(1)
      val expected = sResult.toReadable
      answerServiceMock.create(newAnswer) returns Future { Some(sResult) }
      
      Post(baseUri, newAnswer) ~> routes ~> check {
        val result = getAsEntity[Answer](responseAs[JsValue])
        result shouldBe sResult
        status shouldBe StatusCodes.Created
      }
    }
    
    "return 'BadRequest' for an attempt to create new Answer with invalid JSON" in new DataContext {
      Post(baseUri, """{"content":"test"}""") ~> Route.seal(routes) ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }
    
    "update existing Answer with JSON send via PUT" in new DataContext {
      val sResult = answersList(0).merge(newAnswer)
      val expected = sResult.toReadable
      answerServiceMock.update(answersList(0).id, answersList(0).secret, newAnswer) returns Future { Some(sResult) }
      
      Put(baseUri + "/" + answersList(0).id + "?secret=" + answersList(0).secret, newAnswer) ~> routes ~> check {
        val result = getAsEntity[Answer](responseAs[JsValue])
        result shouldBe sResult
        status shouldBe StatusCodes.OK
      }
    }
    
    "return 'NotFound' for an attempt to update Answer with invalid secret" in new DataContext {
      val sResult = None
      answerServiceMock.update(answersList(0).id, "INVALID_SECRET", newAnswer) returns Future { sResult }
      
      Put(baseUri + "/" + answersList(0).id + "?secret=" + "INVALID_SECRET", newAnswer) ~> Route.seal(routes) ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }
    
    "delete existing Answer" in new DataContext {
      val sResult = true
      answerServiceMock.delete(answersList(0).id, answersList(0).secret) returns Future { sResult }
      
      Delete(baseUri + "/" + answersList(0).id + "?secret=" + answersList(0).secret) ~> routes ~> check {
        status shouldBe StatusCodes.NoContent
      }
    }
    
    "return 'NotFound' for an attempt to delete Answer with invalid secret" in new DataContext {
      val sResult = false
      answerServiceMock.delete(answersList(0).id, "INVALID_SECRET") returns Future { sResult }
      
      Delete(baseUri + "/" + answersList(0).id + "?secret=" + "INVALID_SECRET") ~> Route.seal(routes) ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }
    
  }
  
}