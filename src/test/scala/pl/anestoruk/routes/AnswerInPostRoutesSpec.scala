package pl.anestoruk.routes

import scala.concurrent.Future
import org.scalatest._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import pl.anestoruk._
import pl.anestoruk.models._
import pl.anestoruk.models.PlayJsonFormats._
import pl.anestoruk.utils.PlayJsonMarshallers._
import play.api.libs.json.JsValue

//@Ignore
class AnswerInPostRoutesSpec extends WordSpec with Matchers with ScalatestRouteTest with RoutesContext {
  
  val postId = 1
  val baseUri = uriPrefix + "/posts/" + postId + "/answers"
  
  "Answer within Post routes" should {
    
    "return Answers that belong to specific Post" in new DataContext {
      val post = postsList.find { _.id == postId }
      val sResult = answersList.filter { _.postId == postId }.take(20)
      val expected = sResult.map { _.toReadable }
      answerServiceMock.getAllByPostId(postId, 0, 20) returns Future { sResult }
      postServiceMock.getOne(postId) returns Future { post }
      
      Get(baseUri) ~> routes ~> check {
        val result = getAsList[AnswerReadable](responseAs[JsValue])
        result shouldBe expected
        status shouldBe StatusCodes.OK
      }
    }
    
    "return Answers with 'id', 'before' and 'after' parameters set" in new DataContext {
      val post = postsList
        .find { _.id == postId }
      
      val answer = answersList
        .find { _.postId == postId }
        .get
        
      val sResult = answersList
        .filter { _.postId == postId }
        .take(15)
        
      val expected = sResult.map { _.toReadable }
      answerServiceMock.getAllByPostIdAndAnswerId(postId, answersList(0).id, 5, 10) returns Future { sResult }
      postServiceMock.getOne(postId) returns Future { post }

      Get(baseUri + "?id=" + answersList(0).id + "&before=5&after=10") ~> routes ~> check {
        val result = getAsList[AnswerReadable](responseAs[JsValue])
        result shouldBe expected
        status shouldBe StatusCodes.OK
      }
    }
  }
  
}