package pl.anestoruk.services

import org.scalatest._
import org.scalatest.concurrent._
import org.scalatest.time._
import pl.anestoruk.models._
import pl.anestoruk.models.Types.ID
import pl.anestoruk.ServiceContext

//@Ignore
class PostServiceTest extends FlatSpec with Matchers with ScalaFutures {
  
  implicit val defaultPatience = 
    PatienceConfig(Span(3, Seconds), Span(500, Millis))
  
  "getAll()" should "return Posts" in new ServiceContext {
    setup { _ =>
      whenReady(postService.getAll(0, 20)) { seq =>
        seq.length shouldBe postsList.take(20).length
      }
    }
  }
  
  "get()" should "return Post" in new ServiceContext {
    setup { _ =>
      whenReady(postService.getOne(postsList(0).id)) { maybePost =>
        maybePost shouldBe Some(postsList(0))
      }
    }
  }
  
  "get() with invalid ID" should "return None" in new ServiceContext {
    setup { _ =>
      whenReady(postService.getOne(1337)) { maybePost =>
        maybePost shouldBe None
      }
    }
  }
  
  "create()" should "create new Post" in new ServiceContext {
    setup { _ =>
      whenReady(postService.create(newPost)) { maybePost =>
        val response = maybePost.map { post => 
          PostWritable(post.subject, post.content, post.name, post.email) 
        }
        response shouldBe Some(newPost)
      }
    }
  }
  
  "update()" should "update Post and return it" in new ServiceContext {
    setup { _ =>
      whenReady(postService.update(postsList(0).id, postsList(0).secret, newPost)) { maybePost =>
        val response = maybePost.map { p =>
          PostWritable(p.subject, p.content, p.name, p.email)
        }
        response shouldBe Some(newPost)
      }
    }
  }
  
  "update() with invalid secret" should "not update Post and return None" in new ServiceContext {
    setup { _ =>
      whenReady(postService.update(postsList(0).id, "INVALID_SECRET", newPost)) { maybePost =>
        val response = maybePost.map { p =>
          PostWritable(p.subject, p.content, p.name, p.email)
        }
        response shouldBe None
      }
    }
  }
  
  "delete()" should "remove Post and return true" in new ServiceContext {
    setup { _ =>
      whenReady(postService.delete(postsList(0).id, postsList(0).secret)) { result =>
        result shouldBe true
      }
    }
  }
  
  "delete() with invalid secret" should "not remove Post and return false" in new ServiceContext {
    setup { _ =>
      whenReady(postService.delete(postsList(0).id, "INVALID_SECRET")) { result =>
        result shouldBe false
      }
    }
  }
  
  "getAllByActivity()" should "return Posts sorted by activity" in new ServiceContext {
    setup { _ =>
      whenReady(postService.getAllByActivity(0, 20)) { seq =>
        // we will check order of ids
        val idSeq = seq.map { _.id }
        
        // join Posts (id, createdAt) and Answers (postId, createdAt) data 
        val data = 
          postsList.map { p => (p.id, p.createdAt.getTime) } ++ 
          answersList.map { a => (a.postId, a.createdAt.getTime) }
        
        // group by id, find max createdAt in each group by reducing
        val groupedData = data
          .groupBy { case (id, ts) => id }
          .map { case (id, seq) => 
            seq.reduceLeft { (a, b) => (a._1, a._2 max b._2) }
          }
        
        // sort sequence by createdAt field and get ids
        val expectedSeq = groupedData.toSeq.sortWith { (a, b) => a._2 > b._2 }.map { _._1 } 
        
        idSeq shouldBe expectedSeq
      }
    }
  }
  
  "writable2Entity()" should "convert PostWritable to Post" in new ServiceContext {
    setup { _ =>
      val resultPost = postService.writable2Entity(newPost, 666, "TEST")
      val expectedPost = Post(
          666, 
          newPost.subject, 
          newPost.content, 
          newPost.name, 
          newPost.email, 
          secret = "TEST")
          
      resultPost shouldBe expectedPost
    }
  }
  
  "updateEntity()" should "create updated version of Post" in new ServiceContext {
    setup { _ =>
      val resultPost = postService.updateEntity(postsList(0), newPost)
      val expectedPost = Post(
          postsList(0).id, 
          newPost.subject, 
          newPost.content, 
          newPost.name, 
          newPost.email, 
          postsList(0).createdAt, 
          postsList(0).secret)
          
      resultPost shouldBe expectedPost
    }
  }
  
}
