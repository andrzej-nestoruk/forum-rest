package pl.anestoruk.services

import org.scalatest._
import org.scalatest.concurrent._
import org.scalatest.time._
import pl.anestoruk.models._
import pl.anestoruk.ServiceContext

//@Ignore
class AnswerServiceTest extends FlatSpec with Matchers with ScalaFutures {
  
  implicit val defaultPatience = 
    PatienceConfig(Span(3, Seconds), Span(500, Millis))
  
  "getAll()" should "return Answers" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.getAll(0, 20)) { seq =>
        seq.length shouldBe answersList.take(20).length
      }
    }
  }
  
  "get()" should "return Answer" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.getOne(answersList(0).id)) { maybeAnswer =>
        maybeAnswer shouldBe Some(answersList(0))
      }
    }
  }
  
  "get() with invalid ID" should "return None" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.getOne(1337)) { maybeAnswer =>
        maybeAnswer shouldBe None
      }
    }
  }
  
  "create()" should "create new Answer" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.create(newAnswer)) { maybeAnswer =>
        val response = maybeAnswer.map { answer => 
          AnswerWritable(Some(answer.postId), answer.content, answer.name, answer.email)
        }
        response shouldBe Some(newAnswer)
      }
    }
  }
  
  "update()" should "update Answer and return it" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.update(answersList(0).id, answersList(0).secret, newAnswer)) { maybeAnswer =>
        val response = maybeAnswer.map { a =>
          AnswerWritable(Some(a.postId), a.content, a.name, a.email)
        }
        response shouldBe Some(newAnswer)
      }
    }
  }
  
  "update() with invalid secret" should "not update Answer and return None" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.update(answersList(0).id, "INVALID_SECRET", newAnswer)) { maybeAnswer =>
        val response = maybeAnswer.map { a =>
          AnswerWritable(Some(a.postId), a.content, a.name, a.email)
        }
        response shouldBe None
      }
    }
  }
  
  "delete()" should "remove Answer and return true" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.delete(answersList(0).id, answersList(0).secret)) { result =>
        result shouldBe true
      }
    }
  }
  
  "delete() with invalid secret" should "not remove Answer and return false" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.delete(answersList(0).id, "INVALID_SECRET")) { result =>
        result shouldBe false
      }
    }
  }
  
  "getAllByPostId()" should "return Answers for specific Post" in new ServiceContext {
    setup { _ =>
      whenReady(answerService.getAllByPostId(answersList(0).id, 0, 20)) { result =>
        val expected = answersList
          .filter { _.postId == answersList(0).id }
          .take(20)
          .length
          
        result.length shouldBe expected
      }
    }
  }
  
  "writable2Entity()" should "convert AnswerWritable to Answer" in new ServiceContext {
    setup { _ =>
      val resultAnswer = answerService.writable2Entity(newAnswer, 666, "TEST")
      val expectedAnswer = Answer(
          666, 
          newAnswer.postId.get, 
          newAnswer.content, 
          newAnswer.name, 
          newAnswer.email, 
          secret = "TEST")
          
      resultAnswer shouldBe expectedAnswer
    }
  }
  
  "updateEntity()" should "create updated version of Answer" in new ServiceContext {
    setup { _ =>
      val resultAnswer = answerService.updateEntity(answersList(0), newAnswer)
      val expectedAnswer = Answer(
          answersList(0).id, 
          newAnswer.postId.get,
          newAnswer.content, 
          newAnswer.name, 
          newAnswer.email, 
          answersList(0).createdAt, 
          answersList(0).secret)
          
      resultAnswer shouldBe expectedAnswer
    }
  }
  
}
