package pl.anestoruk

import org.scalatest.Matchers
import org.specs2.mock.Mockito
import pl.anestoruk.routes._
import pl.anestoruk.services._
import play.api.libs.json._
import play.api.libs.json.JsValue.jsValueToJsLookup
import play.api.libs.json._
import play.api.libs.json.Json._

trait RoutesContext extends Mockito with Matchers {
  
  val postServiceMock = mock[PostService]
  val answerServiceMock = mock[AnswerService]
  
  val router = new Router(postServiceMock, answerServiceMock)
  val routes = router.routes
  val uriPrefix = "/v1"
  
  val notFoundError = Json.obj("message" -> "Item not found.")
  
  private val err = (reason: String) => fail(reason)
  
  implicit def handler = router.rejectionHandler
  
  /**
   * 
   */
  def getAsEntity[E : OFormat](json: JsValue) = {
    val data = (json \ "data").getOrElse { err("missing 'data' path!") }
    (data \ "attributes")
      .getOrElse { err("missing 'attributes' path!") }
      .asOpt[E]
      .getOrElse { err("'attributes' path is not a valid Entity") }
  }
  
  /**
   * 
   */
  def getAsList[E : OFormat](json: JsValue) = {
    val array = (json \ "data")
      .getOrElse { err("missing 'data' path!") }
      .asOpt[JsArray]
      .getOrElse { err("'data' path is not an array!") }
      .value
      
    array.map { item =>
      (item \ "attributes")
        .getOrElse { err("missing 'attributes' path!") }
        .asOpt[E]
        .getOrElse { err("'attributes' path is not a valid Entity") }
    }
  }
  
  /**
   * 
   */
  def getAsError(json: JsValue) = {
    (json \ "errors")
      .getOrElse { err("missing 'errors' path!") }
  }
  
}