package pl.anestoruk

import java.sql.Timestamp
import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.util._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time._
import pl.anestoruk.db.H2DBConnection
import pl.anestoruk.models._
import pl.anestoruk.services._
import slick.jdbc.H2Profile.api._

trait ServiceContext extends ScalaFutures with DataContext {
  
  import pl.anestoruk.services.AnswerTable._
  import pl.anestoruk.services.PostTable._
  
  val execContext = ExecutionContext.Implicits.global
  
  implicit val defaultPatience = 
    PatienceConfig(Span(3, Seconds), Span(500, Millis))
  
  val postService = new { 
      val ec = execContext
  } with SlickPostService with H2DBConnection

  val answerService = new {
    val ec = execContext
  } with SlickAnswerService with H2DBConnection
  
  implicit def setupAction = postService.db.run {
      DBIO.seq(
          posts.schema.create,
          answers.schema.create,
          posts ++= postsList,
          answers ++= answersList)
  }
  
  /**
   * 
   */
  def setup(callback: Unit => Any)(implicit future: Future[_]) = {
    whenReady(future) { _ =>
      Try(callback()) match {
        case Failure(e) =>
          postService.db.close
          answerService.db.close
          //println("!")
          throw e
          
        case _ =>
          postService.db.close
          answerService.db.close
          //println(".")
      }
    }
  }
  
}


trait DataContext {
  
  import pl.anestoruk.db.DBData._
  import pl.anestoruk.utils.Utils._
  
  val postsList = {
    (1 to 10).map { i =>
      val name = rand(names)
      Post(i, rand(subjects), rand(contents), name, name2email(name), getTimestamp(day = i - 1))
    }
  }
  
  val answersList = {
    (1 to 20).map { i =>
      val name = rand(names)
      Answer(i, rand(2) + 1, rand(contents), name, name2email(name), getTimestamp(day = i - 1))
    }
  }
  
  val newPost = 
    PostWritable(rand(subjects), rand(contents), rand(names), rand(emails))
      
  val newAnswer = 
    AnswerWritable(Some(1), rand(contents), rand(names), rand(emails))
}