package pl.anestoruk.services

import scala.concurrent.Future
import pl.anestoruk.db.DBConnection
import pl.anestoruk.models._
import pl.anestoruk.models.Types._
import pl.anestoruk.utils.Utils._
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.ExecutionContext

/**
 * @tparam E Entity case class
 * @tparam W EntityWritable case class
 * @tparam T Table case class
 */
trait BasicService[E <: SecretEntity, W] extends DBConnection {
  
  implicit val ec: ExecutionContext
  
  def create(entity: W): Future[Option[E]]
  def getOne(id: ID): Future[Option[E]]
  def getAll(offset: Int, limit: Int): Future[Seq[E]]
  def update(id: ID, secret: String, entity: W): Future[Option[E]]
  def delete(id: ID, secret: String): Future[Boolean]
  
}


/**
 * Provides implementation of basic CRUD methods for Slick Services
 */
trait SlickBasicService[E <: SecretEntity, W, T <: BaseTable[E]] extends BasicService[E, W] {
  
  val data: TableQuery[T]
  
  /**
   * Converts Writable data into full Entity object
   */
  def writable2Entity(writable: W, id: ID = createId, secret: String = createSecret()): E
  
  /**
   * Merges old Entity with Writable data
   */
  def updateEntity(entity: E, writable: W): E
  
  /**
   * Inserts Entity into DB and returns it on success
   */
  def create(item: W): Future[Option[E]] = {
    val entity = writable2Entity(item)
    val action = data.returning(data.map(_.id)) += entity
    db.run(action).map { id => 
      Some(writable2Entity(item, id, entity.secret)) 
    }
  }
  
  /**
   * Returns Entity by ID (if it exists)
   */
  def getOne(id: ID) = {
    val query = data.filter { _.id === id }
    val action = query.result.headOption
    db.run(action)
  }
  
  /**
   * Returns all Entities using provided offset and limit
   */
  def getAll(offset: Int, limit: Int) = {
    val query = data
      .sortBy { _.id.desc }
      .drop(offset)
      .take(limit)
      
    val action = query.result
    db.run(action)
  }
  
  /**
   * Updates Entity if ID and secret are valid and returns it on success
   */
  def update(id: ID, secret: String, updateItem: W) = {
    val newItem = writable2Entity(updateItem, id, secret)
    val query = data.filter { p => p.id === id && p.secret === secret }
    val action = query.update { newItem }
    db.run(action).map { result =>
      if (result > 0) 
        Some(newItem) 
      else 
        None
    }
  }
  
  /**
   * Deletes Entity if ID and secret are valid and returns true on success
   */
  def delete(id: ID, secret: String) = {
    val query = data.filter { p => p.id === id && p.secret === secret }
    val action = query.delete
    db.run(action).map { result =>
      if (result > 0) 
        true 
      else 
        false
    }
  }
  
}


abstract class BaseTable[E <: Entity](tag: Tag, tableName: String) extends Table[E](tag, tableName) {
    def id = column[ID]("ID", O.PrimaryKey, O.AutoInc)
    def secret = column[String]("Secret")
}