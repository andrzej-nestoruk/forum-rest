package pl.anestoruk.services

import java.sql.Timestamp
import scala.concurrent.Future
import akka.http.scaladsl.model.StatusCodes._
import pl.anestoruk.models._
import pl.anestoruk.models.Types.ID
import pl.anestoruk.services.AnswerTable._
import pl.anestoruk.services.PostTable._
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape.proveShapeOf

trait AnswerService extends BasicService[Answer, AnswerWritable] {
  def getAllByPostId(
      postId: ID, offset: Int, limit: Int): Future[Seq[Answer]]
  
  def getAllByPostIdAndAnswerId(
      postId: ID, id: ID, rawBefore: Int, rawAfter: Int): Future[Seq[Answer]]
}

/**
 * Slick implementation of AnswerService
 */
trait SlickAnswerService extends AnswerService with SlickBasicService[Answer, AnswerWritable, SlickAnswerTable] {
  
  val data = answers
  
  /**
   * Gets all Answers that belongs to Post specified by postId
   */
  def getAllByPostId(postId: ID, offset: Int, limit: Int) = {
    val query = answers
      .filter { a => a.postId === postId }
      .sortBy { a => (a.createdAt.asc, a.id.asc) }
      .drop(offset).take(limit)
      
    val action = query.result
    db.run(action)
  }
  
  /**
   * Gets all Answers around Answer that belongs to specific Post
   * 
   * @param postId
   * @param id ID of the Answer that should be in the 'center' point of Before and After values
   * @param before number of Answers BEFORE 'central' Answer 
   * @param after number of Answer AFTER 'central' Answer
   */
  def getAllByPostIdAndAnswerId(postId: ID, id: ID, before: Int, after: Int) = {
    val baseQuery = answers
      .filter { a => a.postId === postId }
      .sortBy { a => (a.createdAt.asc, a.id.asc) }
      
    // gets record index of the Answer, so we can calculate proper offset and limit
    val query1 = baseQuery
      .zipWithIndex
      .filter { a => a._1.id === id }
    
    for {
      answer <- db.run(query1.result.headOption)
      index = answer.map { _._2 }.getOrElse { throw SafeException(NotFound, "Item not found.") }
      offset = if (index - before > 0) (index - before) else 0
      limit = (List(index, before).min + after + 1)
      query2 = baseQuery.drop(offset).take(limit)
      result <- db.run(query2.result)
    } yield { result }
  }
  
  def writable2Entity(writable: AnswerWritable, id: ID, secret: String): Answer = 
    writable.toEntity(id, secret)
  
  def updateEntity(oldEntity: Answer, newEntity: AnswerWritable): Answer = 
    oldEntity.merge(newEntity)
}


/**
 * Slick Table class and TableQuery object
 */
object AnswerTable {
  
  class SlickAnswerTable(tag: Tag) extends BaseTable[Answer](tag, "ANSWERS") {
    def postId = column[Types.ID]("PostID")
    def content = column[String]("Content")
    def name = column[String]("Name")
    def email = column[String]("Email")
    def createdAt = column[Timestamp]("CreatedAt")
    
    def * = (id, postId, content, name, email, createdAt, secret) <> ((Answer.apply _).tupled, Answer.unapply)
    
    // note that CASCADE deleting is turned on!
    def post = foreignKey("PostFK", postId, posts)(_.id, onDelete = ForeignKeyAction.Cascade)
  }
  
  val answers = TableQuery[SlickAnswerTable]

}