package pl.anestoruk.services

import java.sql.Timestamp
import scala.concurrent.Future
import pl.anestoruk.models._
import pl.anestoruk.models.Types._
import pl.anestoruk.services.AnswerTable._
import pl.anestoruk.services.PostTable._
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape.proveShapeOf
import slick.lifted.TableQuery

trait PostService extends BasicService[Post, PostWritable] {
  def getAllByActivity(offset: Int, limit: Int): Future[Seq[Post]]
}

/**
 * Slick implementation of PostService
 */
trait SlickPostService extends PostService with SlickBasicService[Post, PostWritable, SlickPostTable] {
  
  val data = posts
  
  /**
   * Gets all Posts sorted by Activity (descending). Activity is measured by taking into consideration
   * Posts' creation time and their Answers' creation time. Most current Timestamp from either of those,
   * means most Active topic (Post).
   */
  def getAllByActivity(offset: Int, limit: Int) = {
    // Left join Answers to Posts
    val query = for {
      (p, a) <- posts joinLeft answers on (_.id === _.postId)
    } yield (p, a)
    
    // Group by Posts and get max 'createdAt' value
    val query2 = query.groupBy { _._1 }.map { 
      case (post, groupedData) =>
        val timestamps = groupedData.map { pa =>
          pa._2.map { _.createdAt }.getOrElse { pa._1.createdAt }
        }
        (post, timestamps.max)
    }
    
    val query3 = query2.sortBy { d => (d._2.desc, d._1.id.desc) }.map { 
      case (post, ts) => post
    }.drop(offset).take(limit)
    
    val action = query3.result
    db.run(action)
  }
  
  def writable2Entity(writable: PostWritable, id: ID, secret: String): Post = 
    writable.toEntity(id, secret)
    
  def updateEntity(oldEntity: Post, newEntity: PostWritable): Post = 
    oldEntity.merge(newEntity)
}


/**
 * Slick Table class and TableQuery object
 */
object PostTable {
  
  class SlickPostTable(tag: Tag) extends BaseTable[Post](tag, "POSTS") {
    def subject = column[String]("Subject")
    def content = column[String]("Content")
    def name = column[String]("Name")
    def email = column[String]("Email")
    def createdAt = column[Timestamp]("CreatedAt")
    
    def * = (id, subject, content, name, email, createdAt, secret) <> ((Post.apply _).tupled, Post.unapply)
  }
  
  val posts = TableQuery[SlickPostTable]
  
}