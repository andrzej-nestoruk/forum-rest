package pl.anestoruk.utils

import akka.http.scaladsl.marshalling.Marshaller.stringMarshaller
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import akka.http.scaladsl.model.ContentTypeRange.apply
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.server.RejectionError
import akka.http.scaladsl.unmarshalling.Unmarshaller
import akka.http.scaladsl.unmarshalling.Unmarshaller.EnhancedFromEntityUnmarshaller
import akka.util.ByteString
import pl.anestoruk.models._
import play.api.libs.json._

/**
 * Marshaller and Unmarshaller for Play JSON.
 * 
 * Modified version of de.heikoseeberger.akkahttpplayjson library!
 */
object PlayJsonMarshallers {
  
  val jsonStringMarshaller = stringMarshaller(`application/json`)
  
  implicit def marshaller[A: Writes]: ToEntityMarshaller[A] = 
    jsonStringMarshaller
      .compose(Json.stringify)
      .compose(implicitly[Writes[A]].writes)
  
  val jsonStringUnmarshaller = Unmarshaller.byteStringUnmarshaller
    .forContentTypes(`application/json`)
    .mapWithCharset {
      case (ByteString.empty, _) => throw Unmarshaller.NoContentException
      case (data, charset) => data.decodeString(charset.nioCharset.name)
    }
  
  implicit def unmarshaller[A: Reads] = {
    def read(json: JsValue) =
      implicitly[Reads[A]]
        .reads(json)
        .recoverTotal { e =>
          throw RejectionError(
              PlayJsonRejection(JsError.toJson(e)))
        }
    jsonStringUnmarshaller.map(data => read(Json.parse(data)))
  }
  
}