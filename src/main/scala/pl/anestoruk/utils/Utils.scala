package pl.anestoruk.utils

import java.util.UUID

/**
 * Utility methods
 */
object Utils {
  
  def createId: UUID = {
    java.util.UUID.randomUUID
  }
  
  def createSecret(size: Int = 32): String = {
    import scala.util.Random
    Random.alphanumeric.take(size).toList.mkString
  }
  
  /**
   * Returns random element from sequence of Strings
   */
  def rand(data: Seq[String]): String = {
    data(scala.util.Random.nextInt(data.size))
  }
  
  /**
   * Short syntax
   */
  def rand(i: Int): Int = {
    scala.util.Random.nextInt(i)
  }
  
  def name2email(name: String) = {
    name.toLowerCase.replaceAll("""[^\w]""", "") + "@testowy.pl"
  }
  
  /**
   * Returns random Timestamp from 'start' till current Timestamp
   * @param start 
   */
  def randTimestamp(start: Int = 1451602800): java.sql.Timestamp = {
    val time = (System.currentTimeMillis / 1000 - start).toInt
    val ts = (scala.util.Random.nextInt(time) + start) * 1000L
    new java.sql.Timestamp(ts)
  }
  
  def getTimestamp(year: Int = 2017, month: Int = 0, day: Int = 0): java.sql.Timestamp = {
    val validYear = year % 50 + 2000
    val validMonth = month % 12 + 1
    val validDay = day % 28 + 1
    val y = year.toString
    val m = if (validMonth < 10) s"0$validMonth" else s"$validMonth"
    val d = if (validDay < 10) s"0$validDay" else s"$validDay"
    java.sql.Timestamp.valueOf(s"$y-$m-$d 12:00:00")
  }
  
  /**
   * Implicit conversion from UUID to 0, it is used for auto-increment primary keys only!
   */
  implicit def uuid2Int(uuid: UUID): Int = 0
  
  implicit def string2Uuid(str: String): UUID = UUID.fromString(str)
  
  implicit def int2Uuid(i: Int): UUID = UUID.randomUUID
  
}