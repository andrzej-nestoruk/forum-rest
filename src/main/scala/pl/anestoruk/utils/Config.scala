package pl.anestoruk.utils
import com.typesafe.config.ConfigFactory
/** * Mix in this trait for simple access to config settings */
trait Config {
  
  val config = ConfigFactory.load()
  val httpConfig = config.getConfig("http");  val settingsConfig = config.getConfig("settings");  
}