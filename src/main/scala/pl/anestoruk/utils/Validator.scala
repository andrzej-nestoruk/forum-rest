package pl.anestoruk.utils

import scala.util.matching.Regex

/**
 * Field validator trait.
 */
trait Validator[T] {
  
  val fieldValue: T
  
  val fieldName: String
  
  /**
   * Regex matching helper method
   */
  def regexMatch(str: String, regex: Regex): Boolean = {
    str match {
      case regex(_*) => true
      case _ => false
    }
  }
  
}

/**
 * String implementation of field validator
 */
class StringValidator(val fieldValue: String, val fieldName: String) extends Validator[String] {
  
  def minMaxSize(min: Int, max: Int) = {
    val condition = fieldValue.length >= min && fieldValue.size <= max
    val message = s"$fieldName.minMaxSize($min, $max)"
    require(condition, message)
  }
  
  def email = {
    val regex = """^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""".r
    val condition = regexMatch(fieldValue, regex)
    val message = s"$fieldName.invalidEmail" 
    require(condition, message)
  }
  
}

/**
 * Integer implementation of field validator
 */
class IntValidator(val fieldValue: Int, val fieldName: String) extends Validator[Int]

/**
 * Validations should be added using methods from this object!
 */
object validate {
  def apply(fieldValue: String, fieldName: String)(f: (StringValidator) => Unit) = {
    val instance = new StringValidator(fieldValue, fieldName)
    f.apply(instance)
  }
  
  def apply(fieldValue: Int, fieldName: String)(f: (IntValidator) => Unit) = {
    val instance = new IntValidator(fieldValue, fieldName)
    f.apply(instance)
  }
}