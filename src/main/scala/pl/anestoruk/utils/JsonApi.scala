package pl.anestoruk.utils

import scala.concurrent.Future
import scala.util.{ Failure, Success }
import akka.http.scaladsl.marshalling.ToResponseMarshallable.apply
import akka.http.scaladsl.model.StatusCode
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directive._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.StandardRoute
import pl.anestoruk.models._
import pl.anestoruk.models.Types._
import pl.anestoruk.utils.PlayJsonMarshallers._
import pl.anestoruk.routes.CustomDirectives._
import play.api.libs.json._
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import java.sql.SQLException

/**
 * Use this trait to wrap responses containing Entity, lists of Entities, message(s) or errors into JSON format.
 * 
 * General structure of response:
 * {{{
 * {
 *     "status": String,      <- Specifies if response was a success or not, returns either "ok" or "error" value
 *     "code": Int,           <- HTTP status code f.e. "200" on OK, "404" on NotFound
 *     "links": {},           <- Contains (optional) Links Object 
 *     "data": {              <- Successful response contains this field can be either an Object or Array
 *         "attributes: {},   <- Entities data are included in this field
 *         "links: {}         <- Links related to this Entity are located here
 *     },
 *     "errors": {}           <- If response was a Failure it will contain this field instead of "data"
 * }
 * }}}
 * 
 * TODO: Could be improved with strongly typed response format (instead of "dynamic" JSON)
 */
trait JsonApi {
  
  /**
   * Generates JSON response for single Entity object.
   */
  def completeEntity[E <: Entity : OFormat](
      futureItem: Future[Option[E]], 
      status: StatusCode = OK, 
      error: Error = Error(),
      optLinks: Option[Links] = None) = {
    
    extractCleanUri { uri =>
      onComplete(futureItem) {
        case Success(maybeItem) =>
          maybeItem.map { item =>
            val links = linksBuilder(uri, optLinks)
            val data = Json.obj(
                "attributes" -> item,
                "links" -> links)
            val json = Json.obj(
                "status" -> "ok",
                "code" -> status.intValue,
                "links" -> links,
                "data" -> data)
            
            complete(status, json)
          }.getOrElse { completeError(error) }
        
        case Failure(e) => 
          handleServerErrors(e)
      }
    }
  }
  
  /**
   * Generates JSON response for list of Entities.
   */
  def completeList[E <: Entity : OFormat](
      futureSeq: Future[Seq[E]], 
      status: StatusCode = OK, 
      error: Error = Error(),
      optLinks: Option[Links] = None) = {
    
    extractCleanUri { uri =>
      onComplete(futureSeq) {
        case Success(seq) => {
          val data = seq.map { item =>
            val itemLinks = Map("self" -> s"${uri}/${item.id}")
            Json.obj(
                "attributes" -> item,
                "links" -> itemLinks)
          }
          val links = linksBuilder(uri, optLinks)
          val json = Json.obj(
              "status" -> "ok",
              "code" -> status.intValue,
              "links" -> links,
              "data" -> data)
              
          complete(status, json)
        }
        
        case Failure(e) => 
          handleServerErrors(e)
      }
    }
  }
  
  /**
   * Generates single-message JSON response.
   */
  def completeMessage(msg: String) = {
    complete {
      val status = OK
      val data = Json.obj("message" -> msg)
      val json = Json.obj(
          "status" -> "ok",
          "code" -> status.intValue,
          "data" -> data)
      
      (status, json)
    }
  }
  
  /**
   * Generates NoContent response or JSON error on failure.
   */
  def completeBoolean(
      futureItem: Future[Boolean],
      error: Error = Error()) = {
    onComplete(futureItem) {
      case Success(result) =>
        if (result) 
          complete(NoContent) 
        else 
          completeError(error)
        
      case Failure(e) => 
        handleServerErrors(e)
    }
  }
  
  /**
   * Generates JSON error response.
   */
  def completeError(error: Error = Error()) = {
    complete {
      val json = Json.obj(
          "status" -> "error",
          "code" -> error.status.intValue,
          "errors"-> error.data)
          
      (error.status, json)
    }
  }
  
  // *******************************************************************************************************************
  
  def linksBuilder(baseUri: String, optLinks: Option[Links]) = {
    optLinks match {
      case Some(links) =>
        links.map { case (key, value) => 
          (key -> s"${baseUri}${value}")
        }
      case None =>
        Map("self" -> baseUri)
    }
  }
  
  /**
   * Logging can be added here
   */
  def handleServerErrors(e: Throwable): StandardRoute = {
    val internalServerError = Error(InternalServerError, "Something went wrong!")
    e match {
      case e: SafeException =>
        println("ERROR (SERVER): " + e.message)
        completeError(Error(e.status, e.message))
        
      case e: SQLException =>
        println("ERROR (DATABASE): " + e.getMessage)
        if (e.getSQLState == "23503")
          completeError(Error(BadRequest, "Invalid request. Foreign key does not exist."))
        else
          completeError(internalServerError)
        
      case _ =>
        println("ERROR (SERVER): " + e.getMessage)
        completeError(internalServerError)
    }
    
  }
  
}