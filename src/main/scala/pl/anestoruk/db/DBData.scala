package pl.anestoruk.db

import pl.anestoruk.models._
import pl.anestoruk.utils.Utils._

/**
 * Data from this object can be used to populate database with random initial data (also used in Tests)
 */
object DBData {
  
  val subjects = 
    Seq(
        "TEMAT", 
        "Temat", 
        "O co chodzi?", 
        "Kolejny temat",
        "To jest temat...",
        "Tak czy nie?",
        "Pierwszy temat",
        "A to nie pierwszy temat",
        "Witam wszystkich", 
        "Bla Bla Bla")
        
  val contents = 
    Seq(
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." +
        "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." + 
        "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." + 
        "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        
        "Longum odoris putabo juncta de ac si. Nia sae nos magna novas creet. Vita re modo gnum an." +
        "Nemo de sese apti modi suum ut quem. Integritas istiusmodi mem respondeam cui existimavi ita." +
        "Revolvo ea produci pretium eo at liberet ex. Tangitur de facilius quicquid ostendam lectione et ut sensisse." +
        "Illamque cau eae placidae suo externis deinceps. Autho etc ideae hic ita meo situm. Gi ex expectabam diversorum praecipuus me ac.",
        
        "Novi ex utor ac et meis. Agam aër nam cur unum sap sex. Albedinem de quaeretur purgantur ii distingui denegante cerebella. " +
        "Argumenti occurrere usurpabam aliquando quantitas per infinitam vox res. Ordo etsi at loco ab aspi. " +
        "Conversa habeatur articulo ima hoc hic. Fieri docti ex fieri plane tanti lente se. Recorder rum admittit mei mea usitatus ita diversis. " +
        "Ab is similibus an credendas debiliora at. Quia volo sum isti deus vere qui qua vix nemo."
    )
    
  val names =
    Seq(
        "Anna",
        "Maria",
        "Katarzyna",
        "Agnieszka",
        "Krystyna",
        "Barbara",
        "Ewa",
        "Zofia",
        "Janina",
        "Teresa",
        "Joanna",
        "Magdalena",
        "Monika",
        "Jadwiga",
        "Danuta",
        "Irena",
        "Halina",
        "Helena",
        "Beata",
        "Aleksandra",
        "Marta",
        "Dorota",
        "Marianna",
        "Jolanta",
        "Iwona",
        "Karolina",
        "Urszula",
        "Justyna",
        "Renata",
        "Alicja",
        "Paulina",
        "Sylwia",
        "Natalia",
        "Wanda",
        "Agata",
        "Aneta",
        "Izabela",
        "Ewelina",
        "Marzena",
        "Genowefa",
        "Patrycja",
        "Kazimiera",
        "Edyta",
        "Stefania",
        "Jan",
        "Andrzej",
        "Piotr",
        "Krzysztof",
        "Tomasz",
        "Marcin",
        "Marek",
        "Grzegorz",
        "Jerzy",
        "Tadeusz",
        "Adam",
        "Zbigniew",
        "Ryszard",
        "Dariusz",
        "Henryk",
        "Mariusz",
        "Kazimierz",
        "Wojciech",
        "Robert",
        "Mateusz",
        "Marian",
        "Jacek",
        "Janusz",
        "Maciej",
        "Kamil",
        "Roman",
        "Jakub",
        "Artur",
        "Edward",
        "Damian",
        "Dawid",
        "Sebastian",
        "Leszek",
        "Daniel",
        "Waldemar")
        
  val emails = 
    Seq(
        "abc@def.ghi",
        "xyz@email.pl",
        "test@test.test")
        
}