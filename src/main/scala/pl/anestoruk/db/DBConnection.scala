package pl.anestoruk.db

import slick.lifted.TableQuery
import slick.model.Table
import slick.lifted.AbstractTable
import pl.anestoruk.models.Entity
import pl.anestoruk.models.Post

trait DBConnection {
  
  import slick.basic._
  
  val driver: BasicProfile
  val db: driver.api.Database
  
}


trait PostgresDBConnection extends DBConnection {
  
  import slick.jdbc.PostgresProfile
  
  val driver = PostgresProfile
  val db: driver.api.Database = driver.api.Database.forConfig("postgresql")
  
}


trait H2DBConnection extends DBConnection {
  
  import slick.jdbc.H2Profile
  
  val driver = H2Profile
  val db: driver.api.Database = driver.api.Database.forConfig("h2")
  
}