package pl.anestoruk.server

import scala.concurrent.ExecutionContext
import scala.io.StdIn

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.RouteResult.route2HandlerFlow
import akka.stream.ActorMaterializer
import pl.anestoruk.db.DBConnection
import pl.anestoruk.models.{ Answer, Post }
import pl.anestoruk.routes.Router
import pl.anestoruk.services.AnswerTable.answers
import pl.anestoruk.services.PostTable.posts
import pl.anestoruk.utils.{ Config, JsonApi }
import slick.jdbc.PostgresProfile.api._

abstract class HttpServer(router: Router)(
    implicit system: ActorSystem, 
    materializer: ActorMaterializer, 
    executionContext: ExecutionContext) extends Config with JsonApi with DBConnection {
  
  /**
   * Starts HTTP server
   */
  def start = {
    implicit def handler = router.rejectionHandler
    
    val interface = 
      httpConfig.getString("interface")
      
    val port = 
      httpConfig.getInt("port")
      
    val bindingFuture = Http().bindAndHandle(router.routes, interface, port)
    
    bindingFuture.onSuccess {
      case _ =>
        println(s"Server online at http://${interface}:${port}")
        println("Press [ENTER] to stop...")
    }
    
    StdIn.readLine()
    println("Stopping...")
    db.close
    bindingFuture
      .flatMap { _.unbind() }
      .onComplete { _ => system.terminate() }
  }
  
  /**
   * (Re)creates database tables and inserts randomly generated data
   */
  def dbInit = {
    import pl.anestoruk.db.DBData._
    import pl.anestoruk.utils.Utils._
    
    val postsToGenerate = settingsConfig.getInt("postsToGenerate")
    val answersToGenerate = settingsConfig.getInt("answersToGenerate")
    
    val postsList = {
      (1 to postsToGenerate).map { i =>
        val name = rand(names)
        Post(i, rand(subjects), rand(contents), name, name2email(name), getTimestamp(day = i - 1))
      }
    }
    
    val answersList = {
      (1 to answersToGenerate).map { i =>
        val name = rand(names)
        Answer(i, rand(postsToGenerate / 2) + 1, rand(contents), name, name2email(name), getTimestamp(day = i - 1))
      }
    }
    
    println("Initializing DB schemas and data...")
    val setup = DBIO.seq(
        sqlu"""DROP TABLE IF EXISTS "#${posts.baseTableRow.tableName}" CASCADE""",
        sqlu"""DROP TABLE IF EXISTS "#${answers.baseTableRow.tableName}" CASCADE""",
        posts.schema.create, 
        answers.schema.create,
        posts ++= postsList,
        answers ++= answersList)
        
    db.run(setup).map { _ => println("Done!") }.recover { 
      case e => println("ERROR: " + e.getMessage)
    }
  }
  
}