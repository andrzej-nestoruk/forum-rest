package pl.anestoruk

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import pl.anestoruk.db._
import pl.anestoruk.routes.Router
import pl.anestoruk.server.HttpServer
import pl.anestoruk.services._

/**
 * Main entry point
 */
object WebServer {
  
  def main(args: Array[String]) { 
    implicit val system = ActorSystem("system")
    implicit val materializer = ActorMaterializer()
    implicit val execContext = system.dispatcher
    
    // DB connection type
    type DBC = PostgresDBConnection
    
    val postService: PostService = new { 
      val ec = execContext 
    } with SlickPostService with DBC
      
    val answerService: AnswerService = new { 
      val ec = execContext 
    } with SlickAnswerService with DBC
    
    val router = new Router(postService, answerService)
    
    val server = new HttpServer(router) with DBC
    
    // handle application parameters...
    args.find { _.toLowerCase.equals("init") }.map { _ => server.dbInit }
    
    server.start
  }
  
}