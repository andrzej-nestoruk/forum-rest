package pl.anestoruk.routes

import scala.concurrent.ExecutionContext.Implicits.global
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directive._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import pl.anestoruk.models._
import pl.anestoruk.models.PlayJsonFormats._
import pl.anestoruk.models.Types._
import pl.anestoruk.services._
import pl.anestoruk.utils._
import pl.anestoruk.utils.PlayJsonMarshallers._
import pl.anestoruk.routes.CustomDirectives._
import play.api.libs.json.Json
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.ContentTypes

class Router(
    val postService: PostService, 
    answerService: AnswerService) extends JsonApi with Config {
  
  val maxLimit = settingsConfig.getInt("maxLimit")
  val selfLink: Links = Map("self" -> "")
  
  private val exceptionHandler = {
    ExceptionHandler {
      case e: SafeException =>
        println(s"ERROR (SafeException): ${e.message}")
        completeError(Error(e.status, e.message))
    }
  }
  
  /**
   * Wraps default Rejection-responses into JSON format
   */
  def defaultHandler = RejectionHandler.default
    .mapRejectionResponse {
      case res @ HttpResponse(_, _, ent: HttpEntity.Strict, _) =>
        val message = ent.data.utf8String.replaceAll("\"", """\"""")
        val json = Json.obj(
            "status" -> "error",
            "code" -> res.status.intValue,
            "errors"-> Json.obj("message" -> message))
        res.copy(entity = HttpEntity(ContentTypes.`application/json`, Json.stringify(json)))
        
      case x =>
        x
    }
  
  /**
   * Handles custom rejections and use default handler with custom mapping as a fallback
   */
  def rejectionHandler = {
    RejectionHandler.newBuilder()
      .handle {
        case e: ItemNotFoundRejection =>
          val message = s"Item not found (ID: ${e.id})."
          completeError(Error(NotFound, message))
      }
      .handle {
        case e: PlayJsonRejection =>
          completeError(Error(BadRequest, e.json))
      }
      .result.withFallback(defaultHandler)
  }
  
  // *******************************************************************************************************************
  
  /**
   * Main Routing method
   */
  def routes = {
    handleExceptions(exceptionHandler) {
      pathEndOrSingleSlash {
        extractUri { uri =>
          get {
            completeMessage(
                s"""Hello! Available resources: GET ${uri}v1/posts/, GET ${uri}v1/answers/""")
          }
        }
      } ~
      pathPrefix("v1") {
        parameters('offset.as[Int].?(0), 'limit.as[Int].?(20)) { (offset, rawLimit) =>
          val limit = List(rawLimit, maxLimit).min
          pathPrefix("posts") {
            postsRoutes(offset, limit)
          } ~
          pathPrefix("answers") {
            answersRoutes(offset, limit)
          }
        }
      }
    }
  }
  
  /**
   * Posts routes definition
   */
  private def postsRoutes(offset: Int, limit: Int) = {
    pathEndOrSingleSlash {
      get {
        val links = selfLink + ("next" -> s"?offset=${offset + limit}&limit=${limit}")
        val list = postService
          .getAllByActivity(offset, limit)
          .map { _.map { _.toReadable } }
        
        completeList(list, optLinks = Some(links))
      } ~
      post {
        entity(as[PostWritable]) { post =>
          val entity = postService.create(post)
          
          completeEntity(entity, Created, serverError)
        }
      }
    } ~
    pathPrefix(IntNumber) { postId =>
      pathEnd {
        get {
          val links = selfLink + ("answers" -> "/answers")
          val entity = postService
            .getOne(postId)
            .map { _.map { _.toReadable } }
          
          completeEntity(entity, optLinks = Some(links))
        } ~
        parameters('secret) { secret =>
          put {
            entity(as[PostWritable]) { postWritable =>
              val entity = postService.update(
                  postId, secret, postWritable)
                  
              completeEntity(entity)
            }
          } ~
          delete {
            val result = postService.delete(postId, secret)
            
            completeBoolean(result)
          }
        }
      } ~ 
      pathPrefix("answers") {
        postExists(postId, postService) {
          answersNestedRoutes(postId, offset, limit)
        }
      }
    }
  }
  
  /**
   * Answers routes definition
   */
  private def answersRoutes(offset: Int, limit: Int) = {
    pathEndOrSingleSlash {
      get {
        val links = selfLink + ("next" -> s"?offset=${offset + limit}&limit=${limit}")
        val list = answerService
          .getAll(offset, limit)
          .map { _.map { _.toReadable } }
        
        completeList(list, optLinks = Some(links))
      } ~
      post {
        entity(as[AnswerWritable]) { answerWritable =>
          val entity = answerService.create(answerWritable)
          
          completeEntity(entity, Created, serverError)
        }
      }
    } ~
    pathPrefix(IntNumber) { answerId =>
      pathEnd {
        get {
          val entity = answerService
            .getOne(answerId)
            .map { _.map { _.toReadable } }
          
          completeEntity(entity)
        } ~
        parameters('secret) { secret =>
          put {
            entity(as[AnswerWritable]) { answerWritable =>
              val entity = answerService.update(
                  answerId, secret, answerWritable)
              
              completeEntity(entity)
            }
          } ~
          delete {
            val result = answerService.delete(answerId, secret)
            
            completeBoolean(result)
          }
        }
      }
    }
  }
  
  /**
   * Routes for Answers nested within Posts
   */
  private def answersNestedRoutes(postId: ID, offset: Int, limit: Int) = {
    pathEndOrSingleSlash {
      get {
        parameters('id.as[ID].?, 'before.as[Int].?, 'after.as[Int].?) { (oId, oB, oA) =>
          extractIdBeforeAfter(oId, oB, oA, maxLimit) { case (answerId, before, after) =>
            val list = answerService
              .getAllByPostIdAndAnswerId(postId, answerId, before, after)
              .map { _.map { _.toReadable } }
            
            completeList(list)
          } ~ {
            val links = selfLink + ("next" -> s"?offset=${offset + limit}&limit=${limit}")
            val list = answerService
              .getAllByPostId(postId, offset, limit)
              .map { _.map { _.toReadable } }
            
            completeList(list, optLinks = Some(links))
          }
        }
      } ~
      post {
        entity(as[AnswerWritable]) { partialAnswer =>
          val answerWritable = partialAnswer.copy(Some(postId))
          val entity = answerService.create(answerWritable)
          
          completeEntity(entity, Created, serverError)
        }
      }
    } ~
    pathPrefix(IntNumber) { answerId =>
      pathEnd {
        get {
          val entity = answerService
            .getOne(answerId)
            .map { _.map { _.toReadable } }
          
          completeEntity(entity)
        } ~
        parameters('secret) { secret =>
          put {
            entity(as[AnswerWritable]) { partialAnswer =>
              val answerWritable = partialAnswer.copy(Some(postId))
              val entity = answerService.update(
                  answerId, secret, answerWritable)
              
              completeEntity(entity)
            }
          } ~
          delete {
            val result = answerService.delete(answerId, secret)
            
            completeBoolean(result)
          }
        }
      }
    }
  }
  
  // *******************************************************************************************************************
  
  /**
   * Generic message for server side errors
   */
  private val serverError = Error(
      InternalServerError, "Something went wrong.")
  
}