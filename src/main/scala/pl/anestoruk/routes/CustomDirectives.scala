package pl.anestoruk.routes

import scala.util._
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.Directive._
import akka.http.scaladsl.server.StandardRoute._
import akka.http.scaladsl.server.directives.BasicDirectives._
import akka.http.scaladsl.server.directives.FutureDirectives._
import akka.http.scaladsl.server.directives.RouteDirectives._
import akka.http.scaladsl.model.StatusCodes.BadRequest
import pl.anestoruk.models.Types._
import pl.anestoruk.services.PostService
import pl.anestoruk.models._

object CustomDirectives {
  
  /**
   * Provides extracted Answer ID, Before and After values or rejects if any of those is None 
   */
  def extractIdBeforeAfter(
      optAnswerId: Option[ID], 
      optBefore: Option[Int], 
      optAfter: Option[Int],
      maxLimit: Int): Directive1[(ID, Int, Int)] = {
    
    (for {
      answerId <- optAnswerId
      rawBefore <- optBefore
      rawAfter <- optAfter
    } yield {
      val (before, after) = calcBeforeAfter(rawBefore, rawAfter, maxLimit)
      provide(answerId, before, after)
    }).getOrElse { reject }
    
  }
  
  /**
   * Removes query string parameters and strips trailing slash from URI returned by basic extractUri Directive.
   */
  def extractCleanUri: Directive1[String] = {
    extractUri.map { uri =>
      val result = uri.rawQueryString.map { s => 
        uri.toString.stripSuffix(s"?$s") 
      }.getOrElse { uri.toString }
      
      if (result.charAt(result.length - 1) == '/')
        result.substring(0, result.length - 1)
      else
        result 
    }
  }
  
  /**
   * Used to check if Post with specific ID exists
   */
  def postExists(id: ID, postService: PostService): Directive0 = {
    val rejection = ItemNotFoundRejection(id)
    val bool: Directive1[Boolean] = onComplete(postService.getOne(id)).flatMap { 
      case Success(optPost) =>
        optPost match {
          case Some(_) => provide(true)
          case None => reject(rejection)
        }
        
      case Failure(_) =>
        reject
    }
    
    bool.require { _ == true }
  }
  
  /**
   * Calculates Before and After values including max limit.
   */
  def calcBeforeAfter(rawBefore: Int, rawAfter: Int, limit: Int) = {
    val sum = (rawBefore + rawAfter)
    val baLimit = limit - 1
    if (sum > baLimit) {
      val ratio = rawBefore.doubleValue / sum
      val value = (baLimit * ratio)
      val before = (if (value > 1) value.floor else value.ceil).toInt
      val after = baLimit - before
      (before, after)
    } else {
      (rawBefore, rawAfter)
    }
  }
  
}