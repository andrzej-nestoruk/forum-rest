package pl.anestoruk.models

import akka.http.scaladsl.model.{ StatusCode, StatusCodes }
import akka.http.scaladsl.server.Rejection
import play.api.libs.json.{ JsValue, Json }
import play.api.libs.json.Json.toJsFieldJsValueWrapper

/**
 * It is used for building JSON responses containing errors
 */
case class Error(
    status: StatusCode = StatusCodes.NotFound, 
    data: JsValue = Json.obj("message" -> "Item not found."))
    
object Error {
  def apply(status: StatusCode, message: String) = {
    new Error(status, Json.obj("message" -> message))
  }
}


/**
 * Used to hold validation errors that occurred during Play JSON validation process
 */
case class PlayJsonRejection(
    json: JsValue) extends Rejection
    
    
case class ItemNotFoundRejection(
    id: Types.ID) extends Rejection
    
    
/**
 * 'Safe' in SafeException means that this Exception holds message that can be safely shown to end-user,
 * so it shouldn't contain any vulnerable information about system and so on.
 */
case class SafeException(
    status: StatusCode,
    message: String) extends Throwable