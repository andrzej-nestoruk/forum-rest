package pl.anestoruk.models

import java.lang.System.currentTimeMillis
import java.sql.Timestamp
import akka.http.scaladsl.model.StatusCodes._
import pl.anestoruk.models.Types.ID
import pl.anestoruk.utils.Utils._
import pl.anestoruk.utils._

/**
 * Basic Database Answer model
 */
case class Answer(
    id: ID, 
    postId: ID,
    content: String,
    name: String,
    email: String,
    createdAt: Timestamp = new Timestamp(currentTimeMillis),
    secret: String = createSecret()) extends SecretEntity {
  
  /**
   * Merges AnswerWritable data with existing Answer data, can be used after update was made
   */
  def merge(writable: AnswerWritable) = {
    Answer(
        id, 
        writable.postId.getOrElse { postId }, 
        writable.content, 
        writable.name, 
        writable.email, 
        createdAt, 
        secret)
  }
  
  /**
   * Generates a projection to publicly available properties (hiding 'secret' etc.) 
   */
  def toReadable = {
    AnswerReadable(
        id, 
        postId, 
        content, 
        name, 
        email, 
        createdAt)
  }
  
}


/**
 * Writable Answer class, used to model data that is writable by end-user. Properties validation should
 * be specified here
 */
case class AnswerWritable(
    postId: Option[Types.ID],
    content: String,
    name: String,
    email: String) {
  
  validate(content, "content") { i => 
    i.minMaxSize(1, 1024) 
  }
  validate(name, "name" ) { i => 
    i.minMaxSize(3, 32) 
  }
  validate(email, "email" ) { i =>
    i.email
    i.minMaxSize(5, 32) 
  }
  
  /**
   * Converts AnswerWritable to full Answer
   */
  def toEntity(id: ID = createId, secret: String = createSecret()) = {
    Answer(
        id, 
        postId.getOrElse { throw SafeException(BadRequest, "error.postId.missing") }, 
        content, 
        name, 
        email,
        secret = secret)
  }
  
}

/**
 * Readable Answer class, used to hide vulnerable properties from end-users.
 */
case class AnswerReadable(
    id: Types.ID,
    postId: Types.ID,
    content: String,
    name: String,
    email: String,
    createdAt: Timestamp) extends Entity