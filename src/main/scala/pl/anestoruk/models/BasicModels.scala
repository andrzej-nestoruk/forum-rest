package pl.anestoruk.models

/**
 * Aliases for Types commonly used in application
 */
object Types {
  /**
   * This type is used as an ID for database Entities, can be changed to other type (f.e. UUID),
   * but it requires few more changes afterwards. 
   */
  type ID = Int
  
  /**
   * Shortening alias
   */
  type Links = Map[String, String]
}


/**
 * Basic databse Entity model
 */
trait Entity {
  val id: Types.ID
}


trait SecretEntity extends Entity {
  val secret: String
}