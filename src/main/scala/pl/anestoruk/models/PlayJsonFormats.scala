package pl.anestoruk.models

import java.sql.Timestamp
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import java.util.UUID
import pl.anestoruk.utils._

object PlayJsonFormats {
  
  /**
   * java.sql.Timestamp format
   */
  val tsReads: Reads[Timestamp] = {
    ((JsPath \ "value").read[String]).map { Timestamp.valueOf(_) }
  }
  val tsWrites: OWrites[Timestamp] = new OWrites[Timestamp] { 
    def writes(ts: Timestamp) = Json.obj("value" -> ts.toString) 
  }
  implicit val tsFormat = OFormat(tsReads, tsWrites)
  
  /**
   * Posts' formats
   */
  implicit val postFormat = Json.format[Post]
  implicit val postWritableFormat = Json.format[PostWritable]
  implicit val postReadableFormat = Json.format[PostReadable]
  
  /**
   * Answers formats
   */
  implicit val answerFormat = Json.format[Answer]
  implicit val answerWritableFormat = Json.format[AnswerWritable]
  implicit val answerReadableFormat = Json.format[AnswerReadable]
  
  
  //implicit val jsonResponseFormat = Json.format[JSONResponse]
}