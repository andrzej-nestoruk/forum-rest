package pl.anestoruk.models

import java.lang.System.currentTimeMillis
import java.sql.Timestamp
import pl.anestoruk.models.Types._
import pl.anestoruk.utils.Utils._
import pl.anestoruk.utils._

/**
 * Basic Database Post model
 */
case class Post(
    id: ID, 
    subject: String,
    content: String,
    name: String,
    email: String,
    createdAt: Timestamp = new Timestamp(currentTimeMillis),
    secret: String = createSecret()) extends SecretEntity {
  
  /**
   * Merges PostWritable data with existing Post data, can be used after update was made
   */
  def merge(writable: PostWritable) = {
    Post(
        id, 
        writable.subject, 
        writable.content, 
        writable.name, 
        writable.email, 
        createdAt, 
        secret)
  }
  
  /**
   * Generates a projection to publicly available properties (hiding 'secret' etc.) 
   */
  def toReadable = {
    PostReadable(
        id, 
        subject, 
        content, 
        name, 
        email, 
        createdAt)
  }
  
}


/**
 * Writable Post class, used to model data that is writable by end-user. Properties validation should
 * be specified here
 */
case class PostWritable(
    subject: String,
    content: String,
    name: String,
    email: String) {
  
  validate(subject, "subject") { i => 
    i.minMaxSize(1, 120) 
  }
  validate(content, "content") { i => 
    i.minMaxSize(1, 1024) 
  }
  validate(name, "name" ) { i => 
    i.minMaxSize(3, 32) 
  }
  validate(email, "email" ) { i =>
    i.email
    i.minMaxSize(5, 32) 
  }
  
  /**
   * Converts PostWritable to full Post.
   */
  def toEntity(id: ID = createId, secret: String = createSecret()) = {
    Post(
        id, 
        subject, 
        content, 
        name, 
        email,
        secret = secret)
  }
  
}


/**
 * Readable Post class, used to hide vulnerable properties from end-users.
 */
case class PostReadable(
    id: ID,
    subject: String,
    content: String,
    name: String,
    email: String,
    createdAt: Timestamp) extends Entity